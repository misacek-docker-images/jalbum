# [jAlbum] in a docker container

Idea of this container app: have jalbum gui/headless running inside a container and also have possibility to run it on remote node and access it through browser [vnc](NoVNC: https://github.com/novnc/noVNC),

Jalbum converts full size images from youm camera to a nice album with middle size images.

Structure of full size images (container sees them as read only):
``
    container-var-www-html/
        full-size-images/
            climbing/
                2014/
                2015/
            family/
                2014/
                2015/
``

Structure created by jalbum is the same but the overall size is much smaller.

You can use the container in four modes: [passthrough](mode-passthrough), [headless](mode-headless), [local](mode-local) and [remote](#mode-remote)

## Mode: passthrough
In this mode all commands from command line will be run directly in the container.

### Run simple command

    host $ docker-compose run -e MODE=passthrough jalbum ls -l /var/www/html
    -rw-r--r--. 1 jalbum root 610 Dec 14 15:19 album.rss
    drwxr-xr-x. 4 root   root  60 Dec 13 15:01 full-size-images
    host $

### Attach to shell

    host # docker-compose run -e MODE=passthrough jalbum /bin/bash
    MODE: passthrough
    /bin/bash

    jalbum@jalbum:/var/www/html$ pwd
    /var/www/html

    jalbum@jalbum:/var/www/html$ uname -a
    Linux jalbum 3.10.0-693.2.2.el7.x86_64 #1 SMP Tue Sep 12 22:26:13 UTC 2017 x86_64 GNU/Linux

## Mode: headless

Connect your full size images inside the container and let jalbum headlessly create your album in different directory which you can then move directly to the root of the webserver.

### Requirements:

#### full size photos directory
 - `~/photos/climbing`
 - `~/photos/climbing/jalbum-settings.jap` must exist, if you do not have it use [container-var-www-html/full-size-images/jalbum-settings.jap](container-var-www-html/full-size-images/jalbum-settings.jap)

#### resulting album directory
 - ``~/album/climbing``
 - must be writable by user 5001 (`chown 5001 ~/album/climbing`)

#### (optional) web address
- http://<your-server>/climbing

**Resulting structure on web server:**

    <webserver root>/
        full-size-images/
            climbing/ (~/photos/climbing locally)
                jalbum-settings.jap
        climbing/ (~/album/climbing locally)

### Run it:

    $ sudo docker run \
    -e MODE=headless -e DEBUG=1 \
    -v /tmp/album/:/var/www/html \
    -v /home/q/syncthing/fotky/bazar:/var/www/html/full-size-images \
    registry.gitlab.com/misacek/docker-images/jalbum

## Mode: local

### For linux hosts (tested with Ubuntu 14.04)

``
  $ xhost +
  $ docker-compose up
``

### For macos (tested with High Sierra 10.13.6)

* Check [instructions](https://cntnr.io/running-guis-with-docker-on-mac-os-x-a14df6a76efc).

* Install `socat` and `xquartz`.

```
   $ brew install socat
   $ brew cask install xquartz
```

* Allow local connections for Xserver (`xhost +` equivalent)
```
   $ open -a Xquartz
   # Xquartz preferences, Security, Allow connections from network clients
```

* Use socat to create tunnel ($DISPLAY stays as it is, no replacement with real value
```
   $ socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
```

* Run `jalbum` on local display.
```
   # DISPLAY variable is (any of) your ip addresses plus ':0'
   $ docker-compose run -e DISPLAY=<ip>:0 jalbum
```

## Mode: remote
(wip)


[jAlbum]: https:/jalbum.net
